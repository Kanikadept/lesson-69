import React, {useEffect} from 'react';
import './SeriesInfo.css';
import {useDispatch, useSelector} from "react-redux";
import {requestSingleSeries} from "../../store/actions/tvActions";

const SeriesInfo = (props) => {
    const dispatch = useDispatch();
    const series = useSelector(state => state.singleSeries);

    useEffect(() => {
        dispatch(requestSingleSeries(props.match.params.id));
    }, [dispatch, props.match.params.id])

    return ( series &&
            <div className="series-info">
                <div className="series-img">
                    <img src={series.image !== null ? series.image.medium: ''} alt=""/>
                </div>
                <div className="series-content">
                    <p>{series.name}</p>
                    <p>{series.summary}</p>
                </div>
            </div>
    );
};

export default SeriesInfo;