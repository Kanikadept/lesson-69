import './App.css';
import Layout from "./components/Layout/Layout";
import {Switch, Route} from 'react-router-dom';
import SeriesInfo from "./containers/SeriesInfo/SeriesInfo";

const App = () => (
    <div className="App">
        <div className="container">
            <Layout>
                <Switch>
                    <Route path="/" exact component={SeriesInfo}/>
                    <Route path="/show/:id" exact component={SeriesInfo}/>
                </Switch>
            </Layout>
        </div>
    </div>
);

export default App;
