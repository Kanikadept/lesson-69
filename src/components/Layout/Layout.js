import React from 'react';
import Header from "../Header/Header";
import SearchBar from "../SearchBar/SearchBar";

const Layout = (props) => {
    return (
       <>
           <Header />
           <SearchBar />
           <main className="leyout-content">
               {props.children}
           </main>
       </>
    );
};

export default Layout;