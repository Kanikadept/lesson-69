import React, {useEffect, useState} from 'react';
import './SearchBar.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchSeries} from "../../store/actions/tvActions";
import {NavLink} from "react-router-dom";

const SearchBar = () => {
    const dispatch = useDispatch();
    const series = useSelector(state => state.series);

    const [text, setText] = useState('');

    useEffect(() => {
        dispatch(fetchSeries(text));
    }, [dispatch, text])

    const handleChange = (e) => {
        setText(e.target.value);
    }

    return (
        <div className="search-bar">
            <label className="series-label">Search for TV Show: </label>
            <div className="series-input">
                <input onChange={handleChange} type="text"/>
                <div className="series-list">{series.map(s => (
                    <NavLink key={s.show.id} to={"/show/" + s.show.id}>{s.show.name}</NavLink>
                ))}</div>
            </div>
        </div>
    );
};

export default SearchBar;