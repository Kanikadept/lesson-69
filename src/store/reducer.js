import {FETCH_SERIES, REQUEST_SINGLE_SERIES} from "./actions/tvActions";

const initialState = {
    series: [],
    singleSeries: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_SERIES:
            return {...state, series: action.payload};
        case REQUEST_SINGLE_SERIES:
            return {...state, singleSeries: action.payload};
        default:
            return state;
    }

}

export default reducer;