import axios from "axios";


export const FETCH_SERIES = 'FETCH_SERIES';
export const fetchSeriesSuccess = payload => ({type: FETCH_SERIES, payload})
export const fetchSeries = (text) => {
    return async (dispatch) => {
        try {
            const response = await axios.get('http://api.tvmaze.com/search/shows?q=' + text);
            dispatch(fetchSeriesSuccess(response.data));
        } catch (e) {

        }
    }
}
//============================================================
export const REQUEST_SINGLE_SERIES = 'REQUEST_SINGLE_SERIES';
export const requestSingleSeriesSuccess = payload => ({type: REQUEST_SINGLE_SERIES, payload});
export const requestSingleSeries = (id) => {
    return async (dispatch) => {
        try {
            const response = await axios.get('http://api.tvmaze.com/shows/' + id);
            dispatch(requestSingleSeriesSuccess(response.data));
        } catch (e) {

        }
    }
}